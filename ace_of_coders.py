# Defeat the enemy hero in under three minutes.

def buildArmy():
    # Your hero can summon and command allied troops.

    buildOrder = ["soldier", "soldier"]  # "archer", "artillery", "arrow-tower"
    type = buildOrder[len(hero.built) % len(buildOrder)]
    if hero.gold >= hero.costOf(type):
        hero.summon(type)

def commandArmy():
    friends = hero.built
    enemies = hero.findEnemies()
    points = hero.getControlPoints()
    for i, friend in enumerate(friends):
        if friend.health <= 0 or friend.type == "arrow-tower":
            continue
        # Command your army to capture control points.
        # Make sure to choose your control points wisely!

        point = points[i % len(points)]
        if hero.time < 90:
            hero.command(friend, "defend", point.pos)
        else:
            hero.command(friend, "attack", friend.findNearest(enemies))

def controlHero():
    enemies = hero.findEnemies()
    nearestEnemy = hero.findNearest(enemies)
    shouldAttack = hero.time > 90
    # Use your hero's abilities to turn the tide.
    # if shouldAttack: ...


while True:
    buildArmy()
    commandArmy()
    controlHero()
