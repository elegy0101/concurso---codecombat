### Tournament Ace of Coder CodeCombat

Este [concurso](https://codecombat.com/play/ladder/ace-of-coders) permite practicar las actividades que se hicieron en niveles anteriores cuando te registras en CodeCombat.

Si deseas contribuir en seguida vienen las instrucciones sobre como hacerlo.


## Como contribuir :heart:

Este repositorio está bajo una licencia [Creative Commons Attribution-ShareAlike 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.es). Todos son libres de agregar, editar y corregir el tutorial.


## Como editar

El código fuente [está alojado en GitLab](). El flujo de trabajo de [Merge Request de GitLab](https://docs.gitlab.com/ee/gitlab-basics/add-merge-request.html) se utiliza para aceptar y revisar cambios.

El archivo README.md está escrito en [Markdown Mark Up Language](https://docs.gitlab.com/ee/user/markdown.html).

Puedes encontrar discusiones sobre el contenido del repo en el [rastreador de issues de GitLab](https://gitlab.com/elegy0101/concurso---codecombat/issues).


## Primeros pasos y requisitos previos

Para contribuir al repositorio se necesita lo siguiente para comenzar: 

* Una cuenta de GitLab
* En el caso de ediciones complejas, familiarizarse con los conceptos básicos de la línea de comandos de Git.


## Haz fork al repositorio 

Primero haz fork al repositorio de [Concurso - CodeCombat](https://gitlab.com/elegy0101/concurso---codecombat) a tu cuenta personal de GitLab:

![Fork](fork.png)


## Hacer merge request

Para realizar un merge request selecciona una de la opciones como aparece a continuación:

 
![Merge](merge.png)
 
 
## Mayor información y ayuda

GitLab tiene una excelente [documentación](https://docs.gitlab.com/). ¡Puedes consultarla si necesita ayuda!

Para más preguntas, puedes contactarme desde nep.ceballos@gmail.com


¡Gracias por Leer! :relieved: :v: 







